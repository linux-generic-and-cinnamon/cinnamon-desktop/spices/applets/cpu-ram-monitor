CPU+RAM Monitor applet
© Drugwash, 2022-2023
Licence: GPL v2 or later

This applet is a combination between:
- temperature@fevimu
- simple-system-monitor@ariel

It is meant to be used with a Cinnamon Desktop.
Panel height should be enough to hold three lines of text.

Display is as follows:
CPU usage [%]
CPU temperature [°C or F]
RAM usage [GB]
